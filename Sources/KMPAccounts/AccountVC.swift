//
//  File.swift
//
//
//  Created by Nikolaev Vasiliy on 05.06.2021.
//

import UIKit
import DSKit
import SnapKit

class AccountVC: UIViewController {

    var apiClient: AccountApiClient?

    let stack = UIStackView()
    let accountLabel = UILabel()

    override func loadView() {
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 16
        [
            accountLabel,
        ].forEach {
            $0.numberOfLines = 0
            stack.addArrangedSubview($0)
        }

        let view = UIView()
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        view.addSubview(stack)
        stack.snp.makeConstraints {
            $0.width.equalToSuperview().inset(48)
            $0.center.equalToSuperview()
        }
        self.view = view
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
    }

    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        loadData()
    }

    func loadData() {
        apiClient?.loadData { (account) in
            self.setupData(account: account)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Account"
        loadData()
    }

    func setupData(account: AccountModelProtocol) {
        title = "Счет " + account.name
        let color = ThemeManager.shared.currentTheme.getColor(.textPrimary) ?? .black
        accountLabel.attributedText = account
            .amount
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 16).font)
            .color(color)
    }

}
