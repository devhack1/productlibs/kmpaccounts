//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import Foundation

struct AccountApiClient {
    var accountModel: AccountModelProtocol
    func loadData(completion: @escaping (AccountModelProtocol) -> Void) {
        completion(accountModel)
    }
}

struct AccountsDataProvider: AccountsDataProviderProtocol {
    func loadData(completion: @escaping ([AccountModelProtocol]) -> Void) {
        completion([
            AccountModel(name: "Текущий", amount: "123 222, 02 Р"),
            AccountModel(name: "Запасной", amount: "90 112, 04 Р"),
            AccountModel(name: "Накопительный", amount: "542 019, 00 Р"),
            AccountModel(name: "Жены", amount: "200 000, 00 Р"),
        ])
    }
}
