//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import Foundation

public struct AccountModel: AccountModelProtocol {
    public let name: String
    public let amount: String

    public init(
        name: String? = "",
        amount: String? = ""
    ) {
        self.name = name ?? ""
        self.amount = amount ?? ""
    }
}
