//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit

public struct AccountsFactory {
    public init() {}
    public func makeSome(account: AccountModelProtocol) -> UIViewController {
        let vc = AccountVC()
        vc.apiClient = .init(accountModel: account)
        return vc
    }
}

public struct AccountDataProviderFactory {
    public init() {}
    public func makeProvider() -> AccountsDataProviderProtocol {
        return AccountsDataProvider()
    }
}

public protocol AccountModelProtocol {
    var name: String { get }
    var amount: String { get }
}

public class AccountStorage {
    public init() {}
    public func getAccountSumm(completion: @escaping (Double) -> Void) {
        completion(98765432.12)
    }
}

public protocol AccountsDataProviderProtocol {
    func loadData(completion: @escaping ([AccountModelProtocol]) -> Void)
}
