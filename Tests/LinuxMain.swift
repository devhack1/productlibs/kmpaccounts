import XCTest

import KMPAccountsTests

var tests = [XCTestCaseEntry]()
tests += KMPAccountsTests.allTests()
XCTMain(tests)
