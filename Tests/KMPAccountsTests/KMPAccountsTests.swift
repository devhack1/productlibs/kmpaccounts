import XCTest
@testable import KMPAccounts

final class KMPAccountsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(KMPAccounts().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
